Python script to monitor and log system utilization: CPU util, CPU per core util, and RAM util.
 
 Intended to be deployed to a BMS server to monitor multi-user usage and determine potential server bottlenecks.
 
 Python version: 3.7