#Python version: 3.7

'''
	This script is to be deployed to a BMS server scheduled to be run every minute to monitor and benchmark server utilization once the BMS is deployed to the LAN network.
	The utilization logs are appended to a text file.
'''

import psutil
import datetime

currentTime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

cpucoreUtilization = [str(x) + "%" for index, x in enumerate(psutil.cpu_percent(interval=1, percpu=True))]
cpuUtilization = psutil.cpu_percent(interval=1);
ramUtilization = dict(psutil.virtual_memory()._asdict())

cpucoreString = "[" + currentTime + "] " + "CPU per core: " + str(cpucoreUtilization) + "\n"
cpuString = "[" + currentTime + "] " + "CPU: " + str(cpuUtilization) + "%" + "\n"
ramString = "[" + currentTime + "] " + "RAM: " + str(ramUtilization['percent']) + "%" + "\n"

# print(cpucoreString)
f = open("server_cpucore_util.txt","a")
f.write(cpucoreString)
f.close() 

# print(cpuString)
f = open("server_cpu_util.txt","a")
f.write(cpuString)
f.close() 

# print(ramString)
f = open("server_ram_util.txt","a")
f.write(ramString)
f.close() 